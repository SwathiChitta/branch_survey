{
    'name': 'Branch Survey',
    'version': '1.0',
    'category': 'General',
    'description': """
Test Description.
""",
    'author': 'ehAPI Team',
    'maintainer': 'ehAPI',
    'website': 'http://www.ehapi.com',
    'depends': ['base','transtech_module'],
    'data': [
       'wizard/upload_images_view.xml',
       'views/rename_atm_view.xml',
       'views/branch_survey_view.xml',
    ],
    'web': True,
   
    'installable': True,
    'auto_install': False,
    
}
