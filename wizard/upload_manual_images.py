from openerp.osv import fields, osv
from openerp.tools.translate import _
import time

class upload_images(osv.TransientModel):
    _name = "upload.images"

    _columns = {

    'timing_frame_img': fields.binary('Timing Frame Image'),
    'wall_branding_1_img': fields.binary('Wall Branding (1) Image'),
    'wall_branding_2_img': fields.binary('Wall Branding (2) Image'),
    'wall_branding_3_img': fields.binary('Wall Branding (3) Image'),
    'wall_branding_4_img': fields.binary('Wall Branding (4) Image'),
    'wall_branding_5_img': fields.binary('Wall Branding (5) Image'),
    'adv_stand_1_img': fields.binary('Advertising Stand (1) Image'),
    'adv_stand_2_img': fields.binary('Advertising Stand (2) Image'),
    'adv_stand_3_img': fields.binary('Advertising Stand (3) Image'),
    'adv_stand_4_img': fields.binary('Advertising Stand (4) Image'),
    'adv_stand_5_img': fields.binary('Advertising Stand (5) Image'),
    'window_graphic_1_img': fields.binary('Window Graphic (1) Image'),
    'window_graphic_2_img': fields.binary('Window Graphic (2) Image'),
    'window_graphic_3_img': fields.binary('Window Graphic (3) Image'),
    'window_graphic_4_img': fields.binary('Window Graphic (4) Image'),
    'window_graphic_5_img': fields.binary('Window Graphic (5) Image'),
    'window_graphic_6_img': fields.binary('Window Graphic (6) Image'),
    'window_graphic_7_img': fields.binary('Window Graphic (7) Image'),
    'window_graphic_8_img': fields.binary('Window Graphic (8) Image'),
    'window_graphic_9_img': fields.binary('Window Graphic (9) Image'),
    'window_graphic_10_img': fields.binary('Window Graphic (10) Image'),
    'backlit_poster_1_img': fields.binary('Backlit Poster (1) Image'),
    'backlit_poster_2_img': fields.binary('Backlit Poster (2) Image'),
    'backlit_poster_3_img': fields.binary('Backlit Poster (3) Image'),
    'frame_poster_1_img': fields.binary('Frame Poster (1) Image'),
    'frame_poster_2_img': fields.binary('Frame Poster (2) Image'),
    'frame_poster_3_img': fields.binary('Frame Poster (3) Image'),
    'frame_poster_4_img': fields.binary('Frame Poster (4) Image'),
    'frame_poster_5_img': fields.binary('Frame Poster (5) Image'),
    'forex_poster_1_img': fields.binary('Forex Poster (1) Image'),
    'forex_poster_2_img': fields.binary('Forex Poster (2) Image'),
    'forex_poster_3_img': fields.binary('Forex Poster (3) Image'),
    'video_wall_1_img': fields.binary('Video Wall (1) Image'),
    'video_wall_2_img': fields.binary('Video Wall (2) Image'),
    'video_wall_3_img': fields.binary('Video Wall (3) Image'),
    'exchange_price_guide_img': fields.binary('Exchange & Price Guide Image'),
    'led_ad_img': fields.binary('LED Advertisement Image'),
    'vault_sticker_1_img': fields.binary('Vault Sticker (1) Image'),
    'vault_sticker_2_img': fields.binary('Vault Sticker (2) Image'),
    'vault_sticker_3_img': fields.binary('Vault Sticker (3) Image'),
    'vault_sticker_4_img': fields.binary('Vault Sticker (4) Image'),
    'vault_sticker_5_img': fields.binary('Vault Sticker (5) Image'),

    }

    

    def action_upload(self, cr, uid, ids, context=None):
        data = self.read(cr, uid, ids)[0]
        survey_ids = self.pool.get('survey.info').write(cr,uid,context['active_id'],data)
        return {
                'type': 'ir.actions.client',
                'tag': 'reload',  }