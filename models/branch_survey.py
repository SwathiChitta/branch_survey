from datetime import datetime
from operator import itemgetter
from openerp.osv import fields, osv, orm

class BranchSurvey(osv.osv):

	_inherit = 'survey.info'

	_columns = {

	'timing_frame': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded')], 'Timing Frame'),
	'timing_frame_img': fields.binary('Timing Frame Image'),

	'wall_branding_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Wall Branding (1)'),
	'wall_branding_1_img': fields.binary('Wall Branding (1) Image'),

	'wall_branding_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Wall Branding (2)'),
	'wall_branding_2_img': fields.binary('Wall Branding (2) Image'),

	'wall_branding_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Wall Branding (3)'),
	'wall_branding_3_img': fields.binary('Wall Branding (3) Image'),

	'wall_branding_4': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Wall Branding (4)'),
	'wall_branding_4_img': fields.binary('Wall Branding (4) Image'),

	'wall_branding_5': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Wall Branding (5)'),
	'wall_branding_5_img': fields.binary('Wall Branding (5) Image'),

	'adv_stand_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Advertising Stand (1)'),
	'adv_stand_1_img': fields.binary('Advertising Stand (1) Image'),


	'adv_stand_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Advertising Stand (2)'),
	'adv_stand_2_img': fields.binary('Advertising Stand (2) Image'),

	'adv_stand_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Advertising Stand (3)'),
	'adv_stand_3_img': fields.binary('Advertising Stand (3) Image'),

	'adv_stand_4': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Advertising Stand (4)'),
	'adv_stand_4_img': fields.binary('Advertising Stand (4) Image'),

	'adv_stand_5': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Advertising Stand (5)'),
	'adv_stand_5_img': fields.binary('Advertising Stand (5) Image'),

	'window_graphic_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (1)'),
	'window_graphic_1_img': fields.binary('Window Graphic (1) Image'),

	'window_graphic_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (2)'),
	'window_graphic_2_img': fields.binary('Window Graphic (2) Image'),

	'window_graphic_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (3)'),
	'window_graphic_3_img': fields.binary('Window Graphic (3) Image'),

	'window_graphic_4': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (4)'),
	'window_graphic_4_img': fields.binary('Window Graphic (4) Image'),

	'window_graphic_5': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (5)'),
	'window_graphic_5_img': fields.binary('Window Graphic (5) Image'),

	'window_graphic_6': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (6)'),
	'window_graphic_6_img': fields.binary('Window Graphic (6) Image'),

	'window_graphic_7': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (7)'),
	'window_graphic_7_img': fields.binary('Window Graphic (7) Image'),

	'window_graphic_8': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (8)'),
	'window_graphic_8_img': fields.binary('Window Graphic (8) Image'),

	'window_graphic_9': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (9)'),
	'window_graphic_9_img': fields.binary('Window Graphic (9) Image'),

	'window_graphic_10': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Window Graphic (10)'),
	'window_graphic_10_img': fields.binary('Window Graphic (10) Image'),

	'backlit_poster_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Backlit Poster (1)'),
	'backlit_poster_1_img': fields.binary('Backlit Poster (1) Image'),

	'backlit_poster_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Backlit Poster (2)'),
	'backlit_poster_2_img': fields.binary('Backlit Poster (2) Image'),

	'backlit_poster_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Backlit Poster (3)'),
	'backlit_poster_3_img': fields.binary('Backlit Poster (3) Image'),

	'frame_poster_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Frame Poster (1)'),
	'frame_poster_1_img': fields.binary('Frame Poster (1) Image'),

	'frame_poster_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Frame Poster (2)'),
	'frame_poster_2_img': fields.binary('Frame Poster (2) Image'),

	'frame_poster_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Frame Poster (3)'),
	'frame_poster_3_img': fields.binary('Frame Poster (3) Image'),

	'frame_poster_4': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Frame Poster (4)'),
	'frame_poster_4_img': fields.binary('Frame Poster (4) Image'),

	'frame_poster_5': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Frame Poster (5)'),
	'frame_poster_5_img': fields.binary('Frame Poster (5) Image'),

	'forex_poster_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Forex Poster (1)'),
	'forex_poster_1_img': fields.binary('Forex Poster (1) Image'),

	'forex_poster_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Forex Poster (2)'),
	'forex_poster_2_img': fields.binary('Forex Poster (2) Image'),

	'forex_poster_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Forex Poster (3)'),
	'forex_poster_3_img': fields.binary('Forex Poster (3) Image'),

	'video_wall_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Video Wall (1)'),
	'video_wall_1_img': fields.binary('Video Wall (1) Image'),

	'video_wall_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Video Wall (2)'),
	'video_wall_2_img': fields.binary('Video Wall (2) Image'),

	'video_wall_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Video Wall (3)'),
	'video_wall_3_img': fields.binary('Video Wall (3) Image'),

	'exchange_price_guide': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Exchange & Price Guide'),
	'exchange_price_guide_img': fields.binary('Exchange & Price Guide Image'),

	'led_ad': fields.selection([('offline','Offline'),
		('faulty','Faulty'),
		('display-distorted','Display Distorted'),
		('outdated','Outdated Campaign')], 'LED Advertisement'),
	'led_ad_img': fields.binary('LED Advertisement Image'),

	'vault_sticker_1': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Vault Sticker (1)'),
	'vault_sticker_1_img': fields.binary('Vault Sticker (1) Image'),

	'vault_sticker_2': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Vault Sticker (2)'),
	'vault_sticker_2_img': fields.binary('Vault Sticker (2) Image'),

	'vault_sticker_3': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Vault Sticker (3)'),
	'vault_sticker_3_img': fields.binary('Vault Sticker (3) Image'),

	'vault_sticker_4': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Vault Sticker (4)'),
	'vault_sticker_4_img': fields.binary('Vault Sticker (4) Image'),

	'vault_sticker_5': fields.selection([('required','Required'),
		('damaged','Damaged'),
		('faded','Faded'),
		('outdated','Outdated Campaign')], 'Vault Sticker (5)'),
	'vault_sticker_5_img': fields.binary('Vault Sticker (5) Image'),

	}


	def show_image1(self, cr, uid, ids, context=None):
		data = self.read(cr,uid, ids, fields=[context.get('field')], context=context)

		return {
			  'name'     : 'Enlarge Images',
			  'res_model': 'ir.actions.act_url',
			  'type'     : 'ir.actions.act_url',
			  'target'   : 'current',
			  'url'      : 'http://live.transtech.ae/Preview?id=%s&field=%s&branch=yes'%(data[0]['id'],context.get('field'))
			  # 'url'      : 'http://portal.transtech.ae/Enlarge-Image?data=%s'%(data[0][context.get('field')])
		   }

BranchSurvey()

class Custom_Atm_info(osv.osv):

	_inherit = 'atm.info'

	_columns={
	
	'atm_type':fields.selection([('atm_only','ATM only'),
				('atm_cash_deposit','ATM and Cash Deposit'), 
				('drive_through','Drive Through'), 
				('walk_through','Walk Through'), 
				('lobby','Lobby'), 
				('ttw','TTW')],'ATM Type'),
	}